let locationData = [{
	'type': 'Feature',
	'properties': {
		'name': 'Dros',
		'foursquareID': '4c87e933139237047b259d9d'
	},
	'geometry': {
		'type': 'Point',
		'coordinates': [-25.77532, 28.26945]
	}
}, {
	'type': 'Feature',
  'properties': {
		'name': 'Black Bamboo',
		'foursquareID': '52ede535498ec6cd895b7c62'
	},
  'geometry': {
    'type': 'Point',
    'coordinates': [-25.78093, 28.27049]
  }
}, {
	'type': 'Feature',
	'properties': {
		'name': 'Spur',
		'foursquareID': '4c72ae30ad69b60c2c1f84b9'
	},
	'geometry': {
		'type': 'Point',
		'coordinates': [-25.78313, 28.27668]
	}
}, {
	'type': 'Feature',
  'properties': {
		'name': 'Braza',
		'foursquareID': '4c2ddcd6e760c9b695814549'
	},
  'geometry': {
    'type': 'Point',
    'coordinates': [-25.78165, 28.27389]
  }
}, {
	'type': 'Feature',
	'properties': {
		'name': 'McDonalds',
		'foursquareID': '4cadffa7bf70236af5ac04f9'
	},
	'geometry': {
		'type': 'Point',
		'coordinates': [-25.78320, 28.27640]
	}
}, {
	'type': 'Feature',
  'properties': {
		'name': 'KFC',
		'foursquareID': '57df88ac498e93790219722f'
	},
  'geometry': {
    'type': 'Point',
    'coordinates': [-25.77406, 28.26625]
  }
}];
