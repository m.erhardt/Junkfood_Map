// Global variables

let markers = {};
let map;

// build all markers
function buildMarkers() {
	locationData.forEach(function(location) {
		// first we get the Foursquare description for every venue
		let venueID = location.properties.foursquareID;

		function foursquareDescription(venue) {
			let foursquareAPI = 'https://api.foursquare.com/v2/venues/';
			let clientID = 'JCCS4PVLONN5530XR1OBLO1LG0QX2EOA1GSQOIQH3HGOHLGK';
			let clientSecret = 'YILWQ1OQGBJCAK2VIUVRGYYQSQJVOS1X0LK5IIABXEN3Y0I5';
			let v = '20180608';
			let requestURL = foursquareAPI + venue + '/?client_id=' + clientID + '&client_secret=' + clientSecret + '&v=' + v;

			$.getJSON(requestURL, function() {
			}).done(function(data) {
				venueDescription = data.response.venue.description;

				if(venueDescription) {
					createMarker(venueDescription);
				} else {
					createMarker('<em>No description available.</em>');
				}
			}).fail(function(error) {
				$('#map').append('<div class="error"><i class="fas fa-exclamation-circle"></i> Couldn\'t place marker (ID ' + venue + ') because the Foursquare description couldn\'t be loaded. Please reload the page and check the console for further info if the problem persists.</div>');
				console.log(error);
			});
		}

		// make the marker bounce when clicked
		function bounceMarker() {
			this.bounce(2);
		}

		// now we define the markers with a function called after the Ajax request
		function createMarker(foursquareDesc) {
			markers[venueID] = L.marker(location.geometry.coordinates).bindPopup(
				'<h2 class="tooltip-header">' + location.properties.name + '</h2>' +
				'<p>' + foursquareDesc + '</p>'
			).on('click', bounceMarker);

			// and add them to the map
			map.addLayer(markers[venueID]);
		}

		// Now with the markers on the map we call the function to add the Foursquare venue Description
		foursquareDescription(venueID);

	});
}

// make sure the popup show when a location in the navigation is clicked
function openPopupOnMap(id) {
	"use strict";

	locationData.forEach(function(location) {
		// we compare the ID of the clicked venue with the one in the location object
		// and open the correct popup on the map
		let venueID = location.properties.foursquareID;

		if (venueID === id) {
			markers[id].openPopup();
			markers[id].bounce(2);
		}
	});
}

// function to hide/show markers on the map
function updateMapLayer() {
	arr = Object.values(markers);

	arr.forEach(function(marker) {
		// first we remove all markers
		map.removeLayer(marker);

		// and then we add all markers where visibility is set to true
		if (marker.visibility === true) {
			map.addLayer(marker);
		}
	});
}

// initialise map

function initMap() {
	// set the view for the map
	map = L.map('map').setView([-25.7797, 28.2737], 16);

	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap contributors</a>'
	}).addTo(map);

	// add zoom control to the top right
	map.zoomControl.setPosition('topright');

	buildMarkers();
}

// throw error when map doesn't load correctly
function mapError() {
	$('#map').append('<div class="error"><i class="fas fa-exclamation-circle"></i> Oops, something went wrong with the map. Please reload the page.</div>');
}

// KnockoutJS ViewModel
function locationListViewModel() {
	self = this;
	// filter list based on input field
	self.locationFilter = ko.observable('');
	self.locationData = ko.observableArray(locationData);
	self.filterLocationList = ko.computed(function() {
		// return all locations which contain the letters matching the input field
		return self.locationData().filter(
			function(loc){
				return (self.locationFilter().length === 0 || loc.properties.name.toLowerCase().includes(self.locationFilter().toLowerCase()));
			}
		);
	});

	// filter markers depending on input field
	self.filterMarkers = function() {
		self.locationData().forEach(function(loc) {
      // check if the current marker is included in the input field text
      if (self.locationFilter().length === 0 || loc.properties.name.toLowerCase().includes(self.locationFilter().toLowerCase())) {
				// if it is, we add visibility: true to its value in the marker array
				markers[loc.properties.foursquareID].visibility = true;
			} else {
				// and if it isn't, we add visibility: false to its value in the marker array
				markers[loc.properties.foursquareID].visibility = false;
			}
		});
		updateMapLayer();
	}	

	// When a location from the list is clicked we call a function to open the correct popup
	self.openPopupFromList = function(id) {
		openPopupOnMap(id);
	}
}

// toggle nav on click
$('#hamburger').click(function() {
	$('#sidebar').toggleClass('hidden');
});

function init() {
	ko.applyBindings(locationListViewModel());
	initMap();
}

