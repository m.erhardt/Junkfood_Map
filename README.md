# Junkfood map

## Table of Contents

* [About](#about)
* [Installation](#installation)
* [Dependencies](#dependencies)
* [Licence](#licence)

## About

This Junkfood map is the **Neighborhood map** project of _Udacity_'s Front-End Web Developer Nanodegree Program. It shows some places in an specific area in Pretoria, South Africa, where one can enjoy Junkfood.

## Installation

Download the repository and extract the zip file to a folder of your choice. Open the file **index.html** in your browser and find some junkfood!

## Dependencies

All you need is a browser that supports JavaScript and an internet connection to load the external libraries.

## Licence

The code of Junkfood map is licenced under the [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html). For the libraries used in the project, check their licence accordingly.
